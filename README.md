# cedar

**Container-based Electronic Design Automation Runtimes**

This project represents and effort from the Electrical Engineering Department of Stanford University to craft Docker and Singularity-based containers for EDA tools, for use both in research (servers/laptops using Docker) and classes (using Singularity)

Docker-based containers are expected to contain fully open-sourced tool chains for deployment externally and on laptops. For teaching purposes, we will craft containers that either use a full Open Source tool set or a hybrid of commercially-licensed and open source tools. These pages will detail how we build our images, along with the infrastructure to host and utilize them.
